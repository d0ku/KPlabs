/**Pieciociokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

#include <math.h>

class Pieciokat
: public  Figura {

	private:
	 double r;

	Pieciokat(double r){

		string a_1="Bok pieciokata musi byc wiekszy niz zero ";

		if(r<=0)
			throw a_1;

		this->r=r;
		this->pole_licz();
		this->obwod_licz();
	}

	private:

	void pole_licz(){

		this->pole =((this->r*this->r)*(sqrt(5*(5+2*sqrt(5)))))*0.25;
	}

	void obwod_licz(){

		this->obwod=this->r*5;
	}

	public:

	double obwod_zwroc(){
		return this->obwod;
	}

	double pole_zwroc(){
		return this->pole;
	}

};