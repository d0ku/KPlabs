/**Czworokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

using namespace std;


class Czworokat 
: public Figura {

	protected:
	double a;
	double b;
	double c;
	double d;
	double angle;

	public:
	 void czworokat_maker(double a, double b, double c, double d, double angle){

	 	double max=0;
 
 		string a_1="Te liczby nie spelniaja warunku czworokata";
 		string a_2="Jeden z bokow czworokata jest mniejszy od zera";
 		string a_3="Kat czworokata jest bledny: ";

 		if(a>max)
 			max=a;
 		if(b>max)
 			max=b;
 		if(c>max)
 			max=c;
 		if(d>max)
 			max=d;

		if(!( max < a + b + c + d - max))
			throw a_1;

		if(a<=0 || b<=0 || c<=0 || d<=0)
			throw a_2;

		if(angle<=0 || angle>=180)
			throw a_3;

		if(a==c && b==d && a==b && angle==90){     //warunek na kwadrat
			Kwadrat *kwadrat=new Kwadrat(a);
			this->obwod=kwadrat->obwod();
			this->pole=kwadrat->pole();				
		}



		if(a==c && b==d && angle==90){ //warunek na prostokat nr 1

			Prostokat *prostokat = new Prostokat(a,b);
			this->pole=prostokat->pole();
			this->obwod=prostokat->obwod();
		}

		else if (a==b && c==d && angle==90){ // warunek na prostokat nr 2

			Prostokat *prostokat = new Prostokat(a,c);
			this->pole=prostokat->pole();
			this->obwod=prostokat->obwod();
		}

		else if (a==d && c==b && angle==90){ // warunek na prostokat nr 2

			Prostokat *prostokat = new Prostokat(a,c);
			this->pole=prostokat->pole();
			this->obwod=prostokat->obwod();
		}

		if (a==b && c==d && a==c && !(angle==90.0)){

			Romb *romb = new Romb(a,angle);
			this->pole=romb->pole();
			this->obwod=romb->obwod();

		}

		string a_4="Dla takich danych nie ma funkcji liczacej pole i obwod ";

		if(this->obwod==0 && this->pole==0)
			throw a_4;

	}

		double pole_zwroc(){

			return this->pole;
		}

		double obwod_zwroc(){

			return this->obwod;
		}


};