/**Czworokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/




public class Czworokat extends Figura{

	protected double a;
	protected double b;
	protected double c;
	protected double d;
	protected double angle;

	public void czworokat_maker(double a, double b, double c, double d, double angle){
 
		if(!( Math.max(Math.max(a,b),Math.max(c,d)) < a + b + c + d - Math.max(Math.max(a,b),Math.max(c,d))))
			throw new IllegalArgumentException("Te liczby nie spelniaja warunku czworokata");

		if(a<=0 || b<=0 || c<=0 || d<=0)
			throw new IllegalArgumentException("Jeden z bokow czworokata jest mniejszy od zera: ");

		if(angle<=0 || angle>=180)
			throw new IllegalArgumentException("Kat czworokata jest bledny: " + angle);

		if(a==c && b==d && a==b && angle==90){     //warunek na kwadrat
			Kwadrat kwadrat=new Kwadrat(a);
			this.obwod=kwadrat.obwod();
			this.pole=kwadrat.pole();				
		}



		if(a==c && b==d && angle==90){ //warunek na prostokat nr 1

			Prostokat prostokat = new Prostokat(a,b);
			this.pole=prostokat.pole();
			this.obwod=prostokat.obwod();
		}

		else if (a==b && c==d && angle==90){ // warunek na prostokat nr 2

			Prostokat prostokat = new Prostokat(a,c);
			this.pole=prostokat.pole();
			this.obwod=prostokat.obwod();
		}

		else if (a==d && c==b && angle==90){ // warunek na prostokat nr 2

			Prostokat prostokat = new Prostokat(a,c);
			this.pole=prostokat.pole();
			this.obwod=prostokat.obwod();
		}

		if (a==b && c==d && a==c && !(angle==90.0)){

			Romb romb = new Romb(a,angle);
			this.pole=romb.pole();
			this.obwod=romb.obwod();

		}

		if(this.obwod==0 && this.pole==0)
			throw new IllegalArgumentException("Dla takich danych nie ma funkcji liczacej pole i obwod ");

	}

		public double pole()
		{
			return this.pole;
		}

		public double obwod(){

			return this.obwod;
		}


}