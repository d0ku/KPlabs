/**Szesciokat
@author d0ku (Jakub Piątkowski)
@version 1.0
*/

public class Szesciokat extends Figura{

	private double r;

	Szesciokat(double r){

		if(r<=0)
			throw new IllegalArgumentException("Bok szesciokata musi byc wiekszy niz zero : " + r);

		this.r=r;
		this.pole_licz();
		this.obwod_licz();
	}

	private void pole_licz(){

		this.pole =((this.r*this.r)*Math.sqrt(3))*1.5;
	}

	private void obwod_licz(){

		this.obwod=this.r*6;
	}

	public double obwod(){
		return this.obwod;
	}

	public double pole(){
		return this.pole;
	}

}