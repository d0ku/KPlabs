/**Romb
@author d0ku (Jakub Piątkowski)
@version 1.0
*/



public class Romb extends Czworokat{


	Romb(double a, double angle){
		this.a=a;
		this.angle=angle;
		this.pole_licz();
		this.obwod_licz();
	}

	private void pole_licz(){
		if (this.angle==30) 
			this.pole=this.a*this.a*0.5;
			else
		this.pole=this.a*this.a*Math.sin(Math.toRadians(this.angle));

	}

	private void obwod_licz(){

		this.obwod=this.a*4;
	}

	public double obwod(){
		return this.obwod;
	}

	public double pole(){

		return this.pole;
	}





}