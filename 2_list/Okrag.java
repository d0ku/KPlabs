/**Okrag
@author d0ku (Jakub Piątkowski)
@version 1.0
*/


public class Okrag extends Figura{

	private double r;

	Okrag(double r){

		if(r<=0)
			throw new IllegalArgumentException("Promien musi byc wiekszy niz zero : " + r);

		this.r=r;
		pole_licz();
		obwod_licz();
	}

	private void pole_licz(){

		this.pole=Math.PI*r*r;
	}

	private void obwod_licz(){
		this.obwod=2*Math.PI*r;

	}

	public double obwod(){

		return this.obwod;
	}

	public double pole(){

		return this.pole;
	}
	
}