/** class Test_GUI_2, uses Process to run Test using WierszTrojkataPascala in GUI
@author d0ku (Jakub Piątkowski)
@version 1.0
 */

import java.lang.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class ButtonListener implements ActionListener {

	GUIinterface inter;

	public void actionPerformed(ActionEvent e) {
		inter.clear();
		inter.calculate(inter.input.getText(), this.inter);

	}

	ButtonListener(GUIinterface inter) {
		this.inter = inter;
	}

}

class InputListener implements ActionListener {

	GUIinterface inter;

	public void actionPerformed(ActionEvent e) {
		inter.input.setText(e.getActionCommand());
	}
 
	InputListener(GUIinterface inter) {
		this.inter = inter;
	}
}

class GUIinterface {

	JFrame window;
	JPanel panel;
	TextField input;
	TextField output;
	Button button;
	int[] delete_start;
	int count = 0;

	GUIinterface() {
		this.delete_start = new int[34];
		for (int i = 0; i < 34; i++) {
			this.delete_start[i] = 0;
		}
		this.window = new JFrame("Proste GUI dla listy 2");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.panel = new JPanel();
		this.input = new TextField("Tu wprowadz dane", 20);
		this.output = new TextField("Tu beda wyniki", 20);
		this.button = new Button("Wyswietl");
		button.addActionListener(new ButtonListener(this));
		input.addActionListener(new InputListener(this));
		panel.add(input);
		panel.add(button);
		panel.add(output);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

		window.add(panel);
		window.pack();
		window.setLocationRelativeTo(null);
		window.setVisible(true);
	}

	public void clear() {

		if (this.count != 0) {
			int a = this.panel.getComponentCount();

			for (int i = a - 1; i > this.count - 1; i--) {

				this.panel.remove(i);
			}

			this.panel.revalidate();
			this.panel.repaint();
			this.window.pack();
		}
	}

	public void calculate(String b, GUIinterface inter) {
		this.panel.remove(output);
		inter.clear();
		int temporal = 0;
		String text = "./TestC ";
		b = text + b;
		int a = 2;
		char d = ' ';
		this.count = this.panel.getComponentCount();

		try {
			Process p = Runtime.getRuntime().exec(b);
			InputStreamReader xd = new InputStreamReader(p.getInputStream());

			text = "";

			while (a > 0 && a < 255) {
				a = xd.read();

				if (a > 0 && a < 255) {
					d = (char) a;
					System.out.print(d);
					text = text + d;
					if (d == '\n') {
						TextField temp = new TextField(30);

						temporal++;
						temp.setText(text);
						text = "";
						this.panel.add(temp);

					}
				}

			}
			p.destroy();
		} catch (Exception ex) {
		}
		;
		if (temporal == 0) {
			TextField temp = new TextField(30);

			temporal++;
			temp.setText("Podaj argumenty :)");
			text = "";
			this.panel.add(temp);

		}

		this.window.pack();

	}

}

class Test_GUI_2 {

	public static void main(String[] args) {

		GUIinterface inter = new GUIinterface();

	}

}