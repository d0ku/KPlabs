/** Test, odpowiada za main
@author d0ku (Jakub Piątkowski)
@version 1.0
*/
class Test{

	public static void main(String[] args) {

		int tmp=0;
		int n=0;
		int z=0;
		
		try {n=Integer.parseInt(args[0]);} // wyjatek
		catch (NumberFormatException ex) {
			System.out.println(args[0] + " nie jest liczba naturalna");
			return;
		}

		if (n>=34){
			System.out.println(args[0]+ " - zbyt wysoki numer wiersza");
			throw new IllegalArgumentException("Zbyt wysoki numer wiersza:" + n);
		}

		if (n<0){
			System.out.println(args[0]+" - nie istnieje wiersz o takim indeksie");
			throw new IllegalArgumentException("Nie istnieje wiersz o takim indeksie: " +n);
		}

		WierszTrojkataPascala action = new WierszTrojkataPascala(n);

		for (int i=1; i<args.length; i++){
			try {z=Integer.parseInt(args[i]);}
			catch (NumberFormatException ex) {
				System.out.println(args[i] + " nie jest wlasciwym argumentem");
				continue;
			}
			if ((z<0)||(z>n)){
				System.out.println(z + " -> argument spoza przedzialu");
				continue;
			}
			tmp=action.wspolczynnik(z);
			if (tmp==0)
				continue;
			System.out.println(z + " -> " + tmp);
		}

	}

}