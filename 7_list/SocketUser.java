import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

/**
 * class SocketUser, GUI and client-side of a server
 * @author  d0ku (Jakub Piątkowski)
 * @version  1.1
 */
class SocketUser extends JFrame implements ActionListener
{
    JTextField output;
    JButton chooseButton;
    JButton drawButton;
    JTextField input;
    JComboBox<String> type;
    JComboBox<String> operation;
    Socket socket = null;
    PrintWriter out = null;
    BufferedReader in = null;

    /**
     * Constructor for SocketUser
     */
    SocketUser()
    {
        type = new JComboBox<String>();
        type.addItem("Integer");
        type.addItem("Double");
        type.addItem("String");

        operation = new JComboBox<String>();
        operation.addItem("Insert");
        operation.addItem("Delete");
        operation.addItem("Search");
        setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 40));
        input = new JTextField(20);
        output = new JTextField();
        output.setBackground(Color.white);

        drawButton = new JButton("Draw");
        drawButton.addActionListener(this);
        chooseButton = new JButton("Send");
        chooseButton.addActionListener(this);

        setLayout(new FlowLayout());
        add(drawButton);
        add(type);
        add(operation);
        add(input);
        add(chooseButton);
        add(output);
    }

    /**
     * It acts, when elemnt of GUI is activated
     * @param event Event which was used to activate
     */
    public void actionPerformed(ActionEvent event)
    {
        if(event.getSource() == operation)
        {
            if(operation.getSelectedItem() == "Draw")
                input.setEditable(false);
            else
                input.setEditable(true);
        }
        if(event.getSource() == drawButton)
        {
            String to_be_sent = "p";

            to_be_sent = to_be_sent + ":" + type.getSelectedIndex() + ":" + "1";

            out.println(to_be_sent);

            try
            {
                output.setText(in.readLine());
            }
            catch (IOException e)
            {
                System.out.println("Read failed");
                System.exit(1);
            }
            input.setText("");
            input.requestFocus();
            pack();


        }
        if(event.getSource() == chooseButton)
        {
            String to_be_sent = "";

            switch((String)operation.getSelectedItem())
            {
            case "Insert":
            {
                to_be_sent = "i";
                break;
            }
            case "Delete":
            {
                to_be_sent = "d";
                break;
            }

            case "Search":
            {
                to_be_sent = "s";
                break;
            }

            }

            to_be_sent = to_be_sent + ":" + type.getSelectedIndex();
            to_be_sent = to_be_sent + ":" + input.getText();

            Boolean isitsafe = false;

            switch((String)type.getSelectedItem())
            {
            case "Integer":
            {
                try
                {
                    int temp = Integer.parseInt(input.getText());
                    isitsafe = true;
                }
                catch(Exception ex)
                {
                    isitsafe = false;
                    output.setText("Wprowadź poprawny argument");
                    input.setText("");
                    input.requestFocus();
                    pack();
                    return;
                }
                break;
            }

            case "Double":
            {

                try
                {
                    double temp = Double.parseDouble(input.getText());
                    isitsafe = true;
                }
                catch(Exception ex)
                {
                    isitsafe = false;
                    output.setText("Wprowadź poprawny argument");
                    input.setText("");
                    input.requestFocus();
                    pack();
                    return;
                }

                break;
            }
            case "String":
            {
                isitsafe = true;
                break;

            }
            }
            if(to_be_sent.split(":").length == 3 && isitsafe == true)
            {
                out.println(to_be_sent);
            }
            else
            {
                output.setText("Wprowadź argument");
                input.setText("");
                input.requestFocus();
                pack();
                return;
            }


            try
            {
                output.setText(in.readLine());
            }
            catch (IOException e)
            {
                System.out.println("Read failed");
                System.exit(1);
            }
            input.setText("");
            input.requestFocus();
            pack();
        }
    }
    /**
     * Method responsbile for connecting and listening to a Socket
     */
    public void listenSocket()
    {
        try
        {
            socket = new Socket("localhost", 4444);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        }
        catch (UnknownHostException e)
        {
            System.out.println("Unknown host: localhost");
            System.exit(1);
        }
        catch  (IOException e)
        {
            System.out.println("No I/O");
            System.exit(1);
        }
    }
    /**
     * It starts SocketUser
     * @param args doesnt change anything
     */
    public static void main(String[] args)
    {
        SocketUser frame = new SocketUser();
        frame.addWindowListener( new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                System.exit(0);
            }
        } );
        frame.pack();
        frame.setVisible(true);
        frame.listenSocket();
    }
}