#include <iostream>

using namespace std;

template <class T>
class Onepoint
{

private:

    string classname = "";
    T value;
    Onepoint<T> *parent = NULL;
    Onepoint<T> *left_son = NULL;
    Onepoint<T> *right_son = NULL;

public:

    Onepoint(T temp, string typeParameterClass)
    {
        this->classname = typeParameterClass;
        this->value = temp;
    }

    T getValue()
    {
        return this->value;
    }

    string getClassNameCustom()
    {
        return this->classname;
    }

    void setParent(Onepoint<T> *temp)
    {
        this->parent = temp;
    }

    void setLeftSon(Onepoint<T> *temp)
    {
        this->left_son = temp;
        if(temp != NULL)
            temp->setParent(this);
    }

    void setRightSon(Onepoint<T> *temp)
    {
        this->right_son = temp;
        if(temp != NULL)
            temp->setParent(this);
    }

    Onepoint<T> *getParent()
    {
        return this->parent;
    }

    Onepoint<T> *getLeftSon()
    {
        return this->left_son;
    }

    Onepoint<T> *getRightSon()
    {
        return this->right_son;
    }

};

template <class T>
class Bst
{


private:
    Onepoint<T> *base = NULL;
    string to_be_sent = " ";

public:
    Onepoint<T> *searchAndReturn(T valueclear)
    {
        Onepoint<T> *value = new Onepoint<T>(valueclear, "NOTINPORTANT");

        if(this->search(valueclear) == true)
        {

            Onepoint<T> *finder = this->base;

            do
            {

                if(value->getValue() == finder->getValue())
                {
                    return finder;
                }
                else if(value->getValue() > finder->getValue())
                {
                    if(finder->getRightSon() != NULL)
                    {
                        finder = finder->getRightSon();
                    }
                    else
                    {
                        break;
                    }
                }
                else if(value->getValue() < finder->getValue())
                {

                    if(finder->getLeftSon() != NULL)
                    {
                        finder = finder->getLeftSon();
                    }
                    else
                    {
                        break;
                    }
                }


            }
            while(true);  //dangerous part, possible infinite loop
        }
        else
        {
            cout << "nie istnieje" << endl;
            return NULL;
        }
        cout << "Nie znalazłem" << endl;
        return NULL;
    }

    bool search(T valueclear)
    {

        Onepoint<T> *value = new Onepoint<T>(valueclear, "NOTINPORTANT");
        Onepoint<T> *finder = this->base;

        while(true)//dangerous part infinite loop
        {

            if(value->getValue() == finder->getValue())
            {
                return true;
            }
            else if(value->getValue() > finder->getValue())
            {
                if(finder->getRightSon() != NULL)
                {
                    finder = finder->getRightSon();
                }
                else
                {
                    break;
                }
            }
            else if(value->getValue() < finder->getValue())
            {
                if(finder->getLeftSon() != NULL)
                {
                    finder = finder->getLeftSon();
                }
                else
                {
                    break;
                }
            }


        }

        if(finder->getValue() != value->getValue())
        {
            return false;
        }
        else
        {
            return true;
        }


    }

    void remove(T value)
    {

        if(this->search(value) == true)
        {


            Onepoint<T> *to_be_removed = this->searchAndReturn(value);
            if(to_be_removed == NULL)
                cout << "Nie istnieje" << endl;

            if(to_be_removed == this->base)
            {
                Onepoint<T> *temp = this->base;

                if(this->base->getRightSon() == NULL && this->base->getLeftSon() == NULL)
                {
                    this->base = NULL;
                    return;
                }

                if(temp->getRightSon() != NULL)
                {
                    temp = temp->getRightSon();
                }
                else
                {
                    temp = temp->getLeftSon();
                    temp->setParent(NULL);
                    to_be_removed = NULL;
                    this->base = temp;
                    return;

                }

                while(temp->getLeftSon() != NULL)
                {
                    temp = temp->getLeftSon();

                }

                if(temp->getRightSon() != NULL)
                {
                    if(temp->getParent() != this->base)
                    {
                        temp->getParent()->setLeftSon(temp->getRightSon());
                        temp->setParent(NULL);
                        temp->setLeftSon(to_be_removed->getLeftSon());
                        temp->setRightSon(to_be_removed->getRightSon());
                        to_be_removed = NULL;
                        this->base = temp;
                        return;
                    }
                    else
                    {

                        temp->setLeftSon(to_be_removed->getLeftSon());
                        to_be_removed = NULL;
                        temp->setParent(NULL);
                        this->base = temp;
                        return;
                    }

                }



                if(temp->getValue() != to_be_removed->getLeftSon()->getValue())
                    temp->setLeftSon(to_be_removed->getLeftSon());
                if(temp->getValue() != to_be_removed->getRightSon()->getValue())
                    temp->setRightSon(to_be_removed->getRightSon());
                to_be_removed = NULL;
                this->base = temp;
                temp->getParent()->setLeftSon(NULL);
                temp->setParent(NULL);

                return;

            }
            else
            {
                //

                if(to_be_removed->getLeftSon() == NULL && to_be_removed->getRightSon() == NULL )
                {

                    if(to_be_removed->getParent()->getValue() > to_be_removed->getValue())
                    {
                        to_be_removed->getParent()->setLeftSon(NULL);
                        to_be_removed = NULL;
                        return;
                    }
                    else
                    {
                        to_be_removed->getParent()->setRightSon(NULL);
                        to_be_removed = NULL;
                        return;
                    }

                }

                if(to_be_removed->getLeftSon() != NULL && to_be_removed->getRightSon() == NULL)
                {

                    if(to_be_removed->getParent()->getValue() > to_be_removed->getValue())
                    {
                        to_be_removed->getParent()->setLeftSon(to_be_removed->getLeftSon());
                        to_be_removed = NULL;
                        return;
                    }
                    else
                    {
                        to_be_removed->getParent()->setRightSon(to_be_removed->getLeftSon());
                        to_be_removed = NULL;
                        return;
                    }

                }

                if(to_be_removed->getLeftSon() == NULL && to_be_removed->getRightSon() != NULL)
                {

                    if(to_be_removed->getParent()->getValue() > to_be_removed->getValue())
                    {
                        to_be_removed->getParent()->setLeftSon(to_be_removed->getRightSon());
                        to_be_removed = NULL;
                        return;
                    }
                    else
                    {
                        to_be_removed->getParent()->setRightSon(to_be_removed->getRightSon());
                        to_be_removed = NULL;
                        return;
                    }

                }

                if(to_be_removed->getRightSon() != NULL && to_be_removed->getLeftSon() != NULL)
                {

                    Onepoint<T> *temp = to_be_removed;


                    temp = temp->getRightSon();
                    bool happened = false;


                    while(temp->getLeftSon() != NULL)
                    {
                        temp = temp->getLeftSon();
                        happened = true;
                    }

                    if(temp->getRightSon() != NULL && happened)
                    {
                        temp->getParent()->setLeftSon(temp->getRightSon());


                        if(to_be_removed->getParent()->getValue() > temp->getValue() )
                        {
                            to_be_removed->getParent()->setLeftSon(temp);
                        }
                        else
                        {
                            to_be_removed->getParent()->setRightSon(temp);
                        }

                        temp->setLeftSon(to_be_removed->getLeftSon());
                        temp->setRightSon(to_be_removed->getRightSon());
                        to_be_removed = NULL;
                        return;

                    }

                    else if(temp->getRightSon() != NULL)
                    {

                        if(to_be_removed->getParent()->getValue() > temp->getValue() )
                        {
                            to_be_removed->getParent()->setLeftSon(temp);
                        }
                        else
                        {
                            to_be_removed->getParent()->setRightSon(temp);
                        }

                        temp->setLeftSon(to_be_removed->getLeftSon());
                        to_be_removed = NULL;
                        return;

                    }

                    if(to_be_removed->getParent()->getValue() > temp->getValue() )
                    {
                        to_be_removed->getParent()->setLeftSon(temp);
                    }
                    else
                    {
                        to_be_removed->getParent()->setRightSon(temp);
                    }

                    temp->setLeftSon(to_be_removed->getLeftSon());

                    if(temp->getValue() != to_be_removed->getRightSon()->getValue())
                    {
                        temp->setRightSon(to_be_removed->getRightSon());
                    }

                    to_be_removed = NULL;
                    return;

                }


            }


        }
        else
        {
            cout << "Takiego obiektu  nie ma w drzewie" << endl;
        }


    }

    void insert(T value)
    {

        Onepoint<T> *temporal = new Onepoint<T>(value, "NOTIMPORTANT");



        if(this->base == NULL)
        {
            this->base = temporal;
            return;
        }

        Onepoint<T> *nextToCompare = this->base;
        //bool left = true;

        while(true) // dangerous part, could be infinite loop
        {
            if(temporal->getValue() < nextToCompare->getValue())
            {
                if(nextToCompare->getLeftSon() != NULL)
                {
                    nextToCompare = nextToCompare->getLeftSon();
                }
                else
                {
                    nextToCompare->setLeftSon(temporal);
                    return;
                }
            }

            else if(temporal->getValue() > nextToCompare->getValue())
            {

                if(nextToCompare->getRightSon() != NULL)
                {
                    nextToCompare = nextToCompare->getRightSon();
                }
                else
                {
                    nextToCompare->setRightSon(temporal);
                    return;
                }
            }

            else if(temporal->getValue() == nextToCompare->getValue())
            {

                nextToCompare = NULL;
                temporal = NULL;
                return;
            }
        }


    }

    void draw()
    {
        this->insideDraw(this->base);
        cout << endl;

    }

    void insideDraw(Onepoint<T> *location)
    {


        if(location)
        {
            cout << location->getValue() << " ";
            this->insideDraw(location->getLeftSon());
            this->insideDraw(location->getRightSon());
        }

    }

};

int main(int argc, char const *argv[])
{
    Bst<int> *binarytree = new Bst<int>();


    binarytree->insert(13);
    binarytree->insert(8);
    binarytree->insert(12);
    binarytree->insert(3);
    binarytree->insert(7);
    binarytree->insert(9);
    binarytree->draw();
    binarytree->remove(9);
    binarytree->remove(123);
    binarytree->search(9);
    //binarytree.insert(new double("3"));
    //binarytree.insert(new double("6"));
    //binarytree.insert(new Integer("19"));
    // binarytree.insert(new double("1"));
    //binarytree.insert(new String("7"));
    //binarytree.insert(new String("6"));
    //binarytree.insert(new String("8"));
    //binarytree.insert(new String("2"));
    //binarytree.insert(new String("2.5"));
    //binarytree.insert(new String("1.5"));
    //binarytree.insert(1);
    //System.out.println(binarytree.drawToString());
    // binarytree.delete(new Double("3"));
    //System.out.println("tu");
    //System.out.println("here");
    //binarytree.delete(2);
    //binarytree.insert(new Integer("96"));
    //System.out.println("tu");
    //binarytree.delete(69);
    binarytree->draw();
    //binarytree.insert(89);

    //binarytree.delete(8);
    //binarytree.delete(2);

    // System.out.println(binarytree.drawToString());
    return 0;
}