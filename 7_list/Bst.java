import java.util.*;


/**
 * class Onepoint, generates one  BST node and it's methods
 * @author  d0ku (Jakub Piątkowski)
 * @version  1.0
 */
class Onepoint<T> implements Comparable<Onepoint<T>>
{
    private String classname = "";
    private T value;
    private Onepoint<T> parent = null;
    private Onepoint<T> left_son = null;
    private Onepoint<T> right_son = null;

    /**
     * Constructor for Onepoint
     * @param  temp value to be set
     * @param  typeParameterClass class name of the value
     */
    Onepoint(T temp, String typeParameterClass)
    {
        this.classname = typeParameterClass;
        this.value = temp;
    }

    //public void setValue(int temp){
    //  this.value=value;
    //}

    /**
     * Getter for value
     * @return value
     */
    public T getValue()
    {
        return this.value;
    }

    /**
     * Getter for value's class name
     * @return value class name
     */
    public String getClassNameCustom()
    {
        return this.classname;
    }

    /**
     * Sets parent for node, better use setLeftSon() and/or setRightSon()
     * @param temp Onepoint to be parent
     */
    public void setParent(Onepoint<T> temp)
    {
        this.parent = temp;
    }

    /**
     * Setter for Left Son of node
     * @param temp Onepoint to be set as left son
     */
    public void setLeftSon(Onepoint<T> temp)
    {
        this.left_son = temp;
        if(temp != null)
            temp.setParent(this);
    }

    /**
     * Setter for Right Son of node
     * @param temp Onepoint to be set as right son
     */
    public void setRightSon(Onepoint<T> temp)
    {
        this.right_son = temp;
        if(temp != null)
            temp.setParent(this);
    }

    /**
     * Returns parent Onepoint
     * @return parent
     */
    public Onepoint<T> getParent()
    {
        return this.parent;
    }

    /**
    * Returns left sonb Onepoint
    * @return left son
    */
    public Onepoint<T> getLeftSon()
    {
        return this.left_son;
    }

    /**
     * Returns right son Onepoint
     * @return right son
     */
    public Onepoint<T> getRightSon()
    {
        return this.right_son;
    }

    /**
     * Implements Comparable, allows to compare objects
     * @param  o object to be compared
     * @return   1- if o is greater, 0 if o is equal to this and 1 if o is lower
     */
    public int compareTo(Onepoint<T> o)
    {
        if(o.getClassNameCustom() == "java.lang.String")
        {

            if (((String)o.getValue()).compareTo((String)this.getValue()) == 0) return 0;
            if (((String)o.getValue()).compareTo((String)this.getValue()) < 0) return -1;
            if (((String)o.getValue()).compareTo((String)this.getValue()) > 0) return 1;


        }

        else if(o.getClassNameCustom() == "java.lang.Integer")
        {


            if (((Integer)o.getValue()).intValue() < ((Integer)this.getValue()).intValue())
            {
                return 1;

            }
            else if(((Integer)o.getValue()).intValue() == ((Integer)this.getValue()).intValue())
            {
                return 0;
            }
            else if (((Integer)o.getValue()).intValue() > ((Integer)this.getValue()).intValue())
            {
                return -1;
            }
        }
        else if(o.getClassNameCustom() == "java.lang.Double")
        {
            if (((Double)o.getValue()).doubleValue() < ((Double)this.getValue()).doubleValue())
            {
                return 1;

            }
            else if(((Double)o.getValue()).doubleValue() == ((Double)this.getValue()).doubleValue())
            {
                return 0;
            }
            else if (((Double)o.getValue()).doubleValue() > ((Double)this.getValue()).doubleValue())
            {
                return -1;
            }
        }


        return -1;

    }
}

/**
 * class Bst, implements Binary Search Trees and it's basic methods
 * @author d0ku (Jakub Piątkowski)
 * @version 2.1
 */
public class Bst<T>
{


    private Onepoint<T> base;
    private String to_be_sent = " ";


    //needs search to be used for to decide whether it exists
    /**
     * That method returns Onepoint pointer, if object with that value exists in BST
     * @param  valueclear value to search
     * @return            pointer to Onepoint
     */
    public Onepoint<T> searchAndReturn(T valueclear)
    {
        Onepoint<T> value = new Onepoint<T>(valueclear, valueclear.getClass().getName());

        if(this.search(valueclear) == true)
        {

            Onepoint<T> finder = this.base;

            do
            {

                if(value.compareTo(finder) == 0)
                {
                    return finder;
                }
                else if(value.compareTo(finder) == 1)
                {
                    if(finder.getRightSon() != null)
                    {
                        finder = finder.getRightSon();
                    }
                    else
                    {
                        break;
                    }
                }
                else if(value.compareTo(finder) == -1)
                {

                    if(finder.getLeftSon() != null)
                    {
                        finder = finder.getLeftSon();
                    }
                    else
                    {
                        break;
                    }
                }


            }
            while(true);
        }
        else
        {
            System.out.println("nie istnieje");
            to_be_sent = "Taki element nie istnieje w drzewie";
            return null;
        }
        to_be_sent = "Nie znaleziono takiego elementu";
        System.out.println("Nie znalazłem");
        return null;
    }

    /**
     * Checks whether value exists in BST
     * @param  valueclear value to search
     * @return            true if exist, false if doesn't
     */
    public Boolean search(T valueclear)
    {

        Onepoint<T> value = new Onepoint<T>(valueclear, valueclear.getClass().getName());
        Onepoint<T> finder = this.base;

        while(true)//dangerous part infinite loop
        {

            if(value.compareTo(finder) == 0)
            {
                return true;
            }
            else if(value.compareTo(finder) == 1)
            {
                if(finder.getRightSon() != null)
                {
                    finder = finder.getRightSon();
                }
                else
                {
                    break;
                }
            }
            else if(value.compareTo(finder) == -1 )
            {
                if(finder.getLeftSon() != null)
                {
                    finder = finder.getLeftSon();
                }
                else
                {
                    break;
                }
            }


        }

        if(finder.compareTo(value) != 0)
        {
            return false;
        }
        else
        {
            return true;
        }


    }

    /**
     * Deletes value Onepoint
     * @param value value to be deleted
     */
    public void delete(T value)
    {

        if(this.search(value) == true)
        {


            Onepoint<T> to_be_deleted = this.searchAndReturn(value);
            if(to_be_deleted == null)
                System.out.println("Nie istnieje");


            if(to_be_deleted == this.base)
            {
                Onepoint<T> temp = this.base;

                if(this.base.getRightSon() == null && this.base.getLeftSon() == null)
                {
                    this.base = null;
                    return;
                }

                if(temp.getRightSon() != null)
                {
                    temp = temp.getRightSon();
                }
                else
                {
                    temp = temp.getLeftSon();
                    temp.setParent(null);
                    to_be_deleted = null;
                    this.base = temp;
                    return;

                }

                while(temp.getLeftSon() != null)
                {
                    temp = temp.getLeftSon();

                }

                if(temp.getRightSon() != null)
                {
                    if(temp.getParent() != this.base)
                    {
                        temp.getParent().setLeftSon(temp.getRightSon());
                        temp.setParent(null);
                        temp.setLeftSon(to_be_deleted.getLeftSon());
                        temp.setRightSon(to_be_deleted.getRightSon());
                        to_be_deleted = null;
                        this.base = temp;
                        return;
                    }
                    else
                    {

                        temp.setLeftSon(to_be_deleted.getLeftSon());
                        to_be_deleted = null;
                        temp.setParent(null);
                        this.base = temp;
                        return;
                    }

                }



                if(temp.compareTo(to_be_deleted.getLeftSon()) != 0)
                    temp.setLeftSon(to_be_deleted.getLeftSon());
                if(temp.compareTo(to_be_deleted.getRightSon()) != 0)
                    temp.setRightSon(to_be_deleted.getRightSon());
                to_be_deleted = null;
                this.base = temp;
                temp.getParent().setLeftSon(null);
                temp.setParent(null);

                return;

            }
            else
            {


                if(to_be_deleted.getLeftSon() == null && to_be_deleted.getRightSon() == null )
                {

                    if(to_be_deleted.getParent().compareTo(to_be_deleted) == 1 )
                    {
                        to_be_deleted.getParent().setLeftSon(null);
                        to_be_deleted = null;
                        return;
                    }
                    else
                    {
                        to_be_deleted.getParent().setRightSon(null);
                        to_be_deleted = null;
                        return;
                    }

                }

                if(to_be_deleted.getLeftSon() != null && to_be_deleted.getRightSon() == null)
                {

                    if(to_be_deleted.getParent().compareTo(to_be_deleted) == 1 )
                    {
                        to_be_deleted.getParent().setLeftSon(to_be_deleted.getLeftSon());
                        to_be_deleted = null;
                        return;
                    }
                    else
                    {
                        to_be_deleted.getParent().setRightSon(to_be_deleted.getLeftSon());
                        to_be_deleted = null;
                        return;
                    }

                }

                if(to_be_deleted.getLeftSon() == null && to_be_deleted.getRightSon() != null)
                {

                    if(to_be_deleted.getParent().compareTo(to_be_deleted) == 1 )
                    {
                        to_be_deleted.getParent().setLeftSon(to_be_deleted.getRightSon());
                        to_be_deleted = null;
                        return;
                    }
                    else
                    {
                        to_be_deleted.getParent().setRightSon(to_be_deleted.getRightSon());
                        to_be_deleted = null;
                        return;
                    }

                }

                if(to_be_deleted.getRightSon() != null && to_be_deleted.getLeftSon() != null)
                {

                    Onepoint<T> temp = to_be_deleted;


                    temp = temp.getRightSon();
                    Boolean happened = false;


                    while(temp.getLeftSon() != null)
                    {
                        temp = temp.getLeftSon();
                        happened = true;
                    }

                    if(temp.getRightSon() != null && happened)
                    {
                        temp.getParent().setLeftSon(temp.getRightSon());


                        if(to_be_deleted.getParent().compareTo(temp) == 1 )
                        {
                            to_be_deleted.getParent().setLeftSon(temp);
                        }
                        else
                        {
                            to_be_deleted.getParent().setRightSon(temp);
                        }

                        temp.setLeftSon(to_be_deleted.getLeftSon());
                        temp.setRightSon(to_be_deleted.getRightSon());
                        to_be_deleted = null;
                        return;

                    }

                    else if(temp.getRightSon() != null)
                    {

                        if(to_be_deleted.getParent().compareTo(temp) == 1 )
                        {
                            to_be_deleted.getParent().setLeftSon(temp);
                        }
                        else
                        {
                            to_be_deleted.getParent().setRightSon(temp);
                        }

                        temp.setLeftSon(to_be_deleted.getLeftSon());
                        to_be_deleted = null;
                        return;

                    }


                    if(to_be_deleted.getParent().compareTo(temp) == 1 )
                    {
                        to_be_deleted.getParent().setLeftSon(temp);
                    }
                    else
                    {
                        to_be_deleted.getParent().setRightSon(temp);
                    }

                    temp.setLeftSon(to_be_deleted.getLeftSon());

                    if(temp.compareTo(to_be_deleted.getRightSon()) != 0)
                    {
                        temp.setRightSon(to_be_deleted.getRightSon());
                    }

                    to_be_deleted = null;
                    return;

                }


            }


        }
        else
        {
            to_be_sent = "Takiego elementu nie ma w drzewie";
            System.out.println("Takiego obiektu nie ma w drzewie");
        }


    }

    /**
     * Inserts value into BSt
     * @param value value to be inserted
     */
    public void insert(T value)
    {

        Onepoint<T> temporal = new Onepoint<T>(value, value.getClass().getName());



        if(this.base == null)
        {
            this.base = temporal;
            return;
        }

        Onepoint<T> nextToCompare = this.base;
        Boolean left = true;

        while(true) // dangerous part, could be infinite loop
        {
            if(temporal.compareTo(nextToCompare) == -1)
            {
                if(nextToCompare.getLeftSon() != null)
                {
                    nextToCompare = nextToCompare.getLeftSon();
                }
                else
                {
                    nextToCompare.setLeftSon(temporal);
                    return;
                }
            }

            else if(temporal.compareTo(nextToCompare) == 1)
            {

                if(nextToCompare.getRightSon() != null)
                {
                    nextToCompare = nextToCompare.getRightSon();
                }
                else
                {
                    nextToCompare.setRightSon(temporal);
                    return;
                }
            }

            else if(temporal.compareTo(nextToCompare) == 0)
            {

                nextToCompare = null;
                temporal = null;
                return;
            }
        }


    }

    /**
     * Draws tree into terminal
     */
    public void draw()
    {
        this.insideDraw(this.base);
    }

    /**
     * Draws tree to String (to_be_sent)
     * @return tree in String format
     */
    public String drawToString()
    {
        to_be_sent = "";
        this.drawToStringInternal(this.base);
        if(to_be_sent == "")
        {
            return "drzewo jest puste";
        }
        else
        {
            return this.to_be_sent;
        }
    }

    /**
     * Recurrency draw, from provided Onepoint
     * @param location start point to draw
     */
    private void drawToStringInternal(Onepoint<T> location)
    {

        if(location != null)
        {
            to_be_sent = to_be_sent + location.getValue() + " ";
            this.drawToStringInternal(location.getLeftSon());
            this.drawToStringInternal(location.getRightSon());
        }
    }

    /**
    * Recurrency draw, from provided Onepoint
    * @param location start point to draw
    */
    private void insideDraw(Onepoint<T> location)
    {


        if(location != null)
        {
            System.out.print(location.getValue() + " ");
            this.insideDraw(location.getLeftSon());
            this.insideDraw(location.getRightSon());
        }

    }

}