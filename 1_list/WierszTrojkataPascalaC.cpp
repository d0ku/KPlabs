#include <iostream>
#include <string>
using namespace std;

class WierszTrojkataPascalaC{

	public:
	int n;
	int *arr; 

	public:
		int wspolczynnik(int m) {
		if (m<0){
			cout<< "Argument jest mniejszy od zera"<<endl;
			return 0; 
		}
		else if (m>n){
			cout<< "Argument jest większy od ostatniego indeksu"<<endl;
			return 0; 
		}
		return this->arr[m];
	}

	~WierszTrojkataPascalaC(){
		delete[] arr;
	}



	WierszTrojkataPascalaC(int n){
		this->n=n;
		arr = new int[n+1];
		if (n<0){
			cout<< "Nie istnieje wiersz o takim indeksie"<<endl;
			return;
		}
		 
		for (int i=0;i<=n;i++){	
			this->arr[0]=1;
			this->arr[i]=1;

			for (int j=i-1;j>0;j--){	
				this->arr[j]=this->arr[j]+this->arr[j-1];
			}
		}
	}
};
	