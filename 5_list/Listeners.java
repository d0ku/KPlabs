import javax.swing.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.*;




/**
 * class MyMouseMotionListener, responsible for figure previevs and movement
 * 
 * @version 1.1
 * @author d0ku (Jakub Piątkowski)
 */
class MyMouseMotionListener implements MouseMotionListener {
	private Shapes shapes;
	private JInternalFrame internalGUIFrame;
	private DrawingPanel drawingPanel;
	private MouseEvent temporal;
	private MouseEvent last_location;
	private GeneralPath figure_preview = new GeneralPath();
	private int count_backup = 0;
	private int counter = 0;
	private int i_backup = 0;
	private int lasttype = 0;
	private ButtonPressedListener which_button;
	private Boolean proceed = true;
	private Color temporalColor = Color.BLACK;

	/**
	 * responsible for figures movement
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseDragged(MouseEvent e) {
		proceed = true;
		int translatex, translatey;
		Graphics2D temp = drawingPanel.getG2d();
		shapes.drawAllPaths(temp);

		Boolean ifvariable = true;

		if (i_backup == -1)
			ifvariable = true;

		else if (shapes.indexedPath(i_backup).contains(e.getX(), e.getY()) == false
				|| which_button.isButtonReleased() == true)
			ifvariable = true;
		else
			ifvariable = false;

		if (proceed)
			if (shapes.getMode() == 1) {

				if (temporal != null && count_backup == shapes.getCount()) {
					if (ifvariable)
						for (int i = shapes.getCount() - 1; i >= 0; i--) {
							if (shapes.indexedPath(i).contains(e.getX(),
									e.getY())) {
								i_backup = i;
								temporalColor = shapes.getLineColor(i);
								shapes.setLineColor(i, Color.RED);

								translatey = translatex = 0;

								if (Math.abs(e.getX() - temporal.getX()) > 10)
									translatex = -1;
								else
									translatex = e.getX() - temporal.getX();
								if (Math.abs(e.getY() - temporal.getY()) > 10)
									translatey = -1;
								else
									translatey = e.getY() - temporal.getY();

								AffineTransform transform = new AffineTransform();
								transform.translate(translatex, translatey);
								shapes.indexedPath(i).transform(transform);

								break;

							}

						}
					else {
						temporalColor = shapes.getLineColor(i_backup);
						shapes.setLineColor(i_backup, Color.RED);

						translatey = translatex = 0;

						if (Math.abs(e.getX() - temporal.getX()) > 10)
							translatex = -1;
						else
							translatex = e.getX() - temporal.getX();
						if (Math.abs(e.getY() - temporal.getY()) > 10)
							translatey = -1;
						else
							translatey = e.getY() - temporal.getY();
						AffineTransform transform = new AffineTransform();
						transform.translate(translatex, translatey);
						shapes.indexedPath(i_backup).transform(transform);

					}
				}
			}
		count_backup = shapes.getCount();

		shapes.drawAllPaths(temp);

		drawingPanel.setG2d(temp);
		drawingPanel.repaint();
		drawingPanel.revalidate();
		shapes.setLineColor(i_backup, temporalColor);

		temporal = e;

		which_button.setIsButtonReleased(false);
	}

	/**
	 * reponsible for figure previews
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseMoved(MouseEvent e) {

		Graphics2D temp = drawingPanel.getG2d();
		shapes.drawAllPaths(temp);

		if (which_button.whichButtonPressed() == 3) {
			shapes.drawAllPaths(temp);
			figure_preview.reset();
			counter = 0;
			last_location = null;
			shapes.setIndexedPath(new GeneralPath(), shapes.getCount());

		}

		if (shapes.getMode() == 0 && lasttype == shapes.getType()) {

			if (which_button.whichButtonPressed() == 1 && counter == 0) // The
			// Powerful
			// If
			{
				which_button.whichButtonPressedSet(-1);
				last_location = e;
				figure_preview.moveTo(e.getX(), e.getY());

				counter++;
			}

			if (shapes.getMode() == 0 && last_location != null) {

				figure_preview.lineTo(e.getX(), e.getY());
				shapes.drawAllPaths(temp);
				temp.setColor(Color.RED);
				temp.draw(figure_preview);
				temp.drawOval(e.getX(), e.getY(), 10, 10);
				if (shapes.indexedPath(shapes.getCount()) != null) // that part
				{
					// prints the
					// preview of
					// the figure
					
					temp.draw(shapes.indexedPath(shapes.getCount()));
					temp.setColor(Color.BLACK);
				}

				if (shapes.getType() == 1) {

					temp.setColor(Color.RED);
					double r = 2 * Math.sqrt(Math.pow(
							(e.getX() - last_location.getX()), 2)
							+ Math.pow((e.getY() - last_location.getY()), 2));
					temp.drawOval(last_location.getX() - (int) r / 2,
							last_location.getY() - (int) r / 2, (int) r,
							(int) r);
					temp.setColor(Color.BLACK);
				}

				if (shapes.getType() == 2) {
					temp.setColor(Color.RED);
					if (last_location.getX() < e.getX()
							&& last_location.getY() < e.getY())
						temp.drawRect(last_location.getX(),
								last_location.getY(),
								Math.abs(last_location.getX() - e.getX()),
								Math.abs(last_location.getY() - e.getY()));
					if (last_location.getX() > e.getX()
							&& last_location.getY() < e.getY())
						temp.drawRect(e.getX(), last_location.getY(),
								Math.abs(last_location.getX() - e.getX()),
								Math.abs(last_location.getY() - e.getY()));
					if (last_location.getX() < e.getX()
							&& last_location.getY() > e.getY())
						temp.drawRect(last_location.getX(), e.getY(),
								Math.abs(last_location.getX() - e.getX()),
								Math.abs(last_location.getY() - e.getY()));
					if (last_location.getX() > e.getX()
							&& last_location.getY() > e.getY())
						temp.drawRect(e.getX(), e.getY(),
								Math.abs(last_location.getX() - e.getX()),
								Math.abs(last_location.getY() - e.getY()));
					temp.setColor(Color.BLACK);
				}

				if (which_button.whichButtonPressed() == 1) {
					counter = 0;
					which_button.whichButtonPressedSet(-1);
					last_location = e;
				}
				figure_preview.reset();
				figure_preview.moveTo(last_location.getX(),
						last_location.getY());

				if (count_backup != shapes.getCount()) {
					shapes.drawAllPaths(temp);
					figure_preview.reset();
					counter = 0;
					last_location = null;
					count_backup = shapes.getCount();
				}

			}
		} else {
			last_location = null;
			counter = 0;
			shapes.drawAllPaths(temp);
			which_button.whichButtonPressedSet(-1);
			lasttype = shapes.getType();
		}

		drawingPanel.setG2d(temp);
		drawingPanel.repaint();
		drawingPanel.revalidate();
	}

	/**
	 * Constructor for MouseMotionListener
	 * 
	 * @param drawingPanel
	 *            custom panel to get Graphics2D from
	 * @param internalGUIFrame
	 *            InternalGUIFrame to draw dialogs on
	 * @param shapes
	 *            Shapes object for figures processing
	 * @param which_button
	 *            ButtonPressedListener to get pressed button
	 */
	MyMouseMotionListener(DrawingPanel drawingPanel,
			JInternalFrame internalGUIFrame, Shapes shapes,
			ButtonPressedListener which_button) {
		this.which_button = which_button;
		this.shapes = shapes;
		this.drawingPanel = drawingPanel;
		this.internalGUIFrame = internalGUIFrame;

	}
}

/**
 * class ButtonPressedListener, which mouse button clicked, and if is button
 * pressed info
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.0
 */
class ButtonPressedListener implements MouseListener {
	private int which_button = -1;
	private Boolean isbuttonreleased = false;
	private int counter = 0;
	private MouseEvent previousstate;
	private MouseEvent actualstate;

	/**
	 * sets which_button variable
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseClicked(MouseEvent e) {

		if (e.getButton() == MouseEvent.BUTTON1)
			which_button = 1;
		if (e.getButton() == MouseEvent.BUTTON2)
			which_button = 2;
		if (e.getButton() == MouseEvent.BUTTON3)
			which_button = 3;
	}

	/**
	 * Getter for last pressed button
	 * 
	 * @return last pressed button
	 */
	public int whichButtonPressed() {
		return this.which_button;
	}

	/**
	 * Setter, sets whethter mouse button is released
	 * 
	 * @param temp
	 *            value to set
	 */
	public void setIsButtonReleased(Boolean temp) {
		this.isbuttonreleased = temp;
	}

	/**
	 * Getter, return whether mouse button is released
	 * 
	 * @return true if released, else if not
	 */
	public Boolean isButtonReleased() {
		return this.isbuttonreleased;
	}

	/**
	 * Setter, sets which button is pressed
	 * 
	 * @param temp
	 *            button number to be set
	 */
	public void whichButtonPressedSet(int temp) {
		this.which_button = temp;
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {

	}

	/**
	 * Sets isbuttonreleased value to true, if button is released
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseReleased(MouseEvent e) {
		this.isbuttonreleased = true;
		// counter=0;
		// System.out.println(e);
	}
}

/**
 * class DrawingMouseListener
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.3
 */
class DrawingMouseListener implements MouseListener {
	private Shapes shapes;
	private Point[] points = new Point[100];
	private int count = 0;
	private DrawingPanel drawingPanel;
	private GeneralPath way = new GeneralPath();
	private JInternalFrame internalGUIFrame;

	public void mouseExited(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {

	}

	/**
	 * adds the point to path, when 1 mouse button clicked, resets whole path,
	 * if 3 mouse button clicked
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseClicked(MouseEvent e) {

		Graphics2D temp = drawingPanel.getG2d();

		if (e.getButton() == MouseEvent.BUTTON3) {
			count = 0;

			way = new GeneralPath();
		}

		if (e.getButton() == MouseEvent.BUTTON1) {

			if (shapes.getSafe() == false) {
				shapes.setSafe(true);
				way.reset();
				count = 0;
				way = null;
				way = new GeneralPath();
				// count = 0;
			}

			if (count == 0 && shapes.getMode() == 0) {
				points[0] = e.getPoint();
				way.moveTo(e.getPoint().getX(), e.getPoint().getY());
			}

			if (shapes.getType() == 2 && shapes.getMode() == 0) {

				points[count] = e.getPoint();
				count++;
				way.lineTo(e.getPoint().getX(), e.getPoint().getY());

				if (count == 2) {
					shapes.addGeneralPath(
							shapes.makeNewGeneralPathRectangle(points), 2,
							Color.BLACK, Color.ORANGE);
					count = 0;
					// way.reset();
					way = null;
					way = new GeneralPath();
				}
			}

			if (shapes.getType() == 1 && shapes.getMode() == 0) {

				points[count] = e.getPoint();
				count++;
				way.lineTo(e.getPoint().getX(), e.getPoint().getY());
				if (count == 2) {

					shapes.addGeneralPath(
							shapes.makeNewGeneralPathOval(points), 1,
							Color.BLACK, Color.GREEN);
					count = 0;
					// way.reset();
					way = null;
					way = new GeneralPath();
				}

			}

			if (shapes.getType() == 0 && shapes.getMode() == 0) {

				points[count] = e.getPoint();
				count++;

				if (Math.abs(points[0].getX() - e.getPoint().getX()) < 10
						&& Math.abs(points[0].getY() - e.getPoint().getY()) < 10
						&& count > 1) {
					way.closePath();
					shapes.addGeneralPath(way, 2, Color.BLACK, Color.BLUE);
					count = 0;
					// way.reset();
					way = null;
					way = new GeneralPath();

				}

				if (count != 0) {

					way.lineTo(e.getPoint().getX(), e.getPoint().getY());
					shapes.setIndexedPath(way, shapes.getCount());
					temp.draw(way);
				}
			}

		}

		String colorchoose = " ";
		if (e.getButton() == MouseEvent.BUTTON3 && shapes.getMode() == 1
				&& shapes.getCount() > 0) {

			for (int i = 0; i < shapes.getCount(); i++) {
				if (shapes.indexedPath(i).contains(e.getX(), e.getY())) {
					Object[] possibilities = { "zolty", "czerwony",
							"niebieski", "czarny", "rozowy", "pomaranczowy" };
					colorchoose = (String) JOptionPane.showInputDialog(
							internalGUIFrame, "Wybierz kolor dla tej figury:",
							"Wybor koloru", JOptionPane.PLAIN_MESSAGE,
							UIManager.getIcon("FileView.directoryIcon"),
							possibilities, "czarny");
					if (colorchoose == null)
						colorchoose = " ";

					switch (colorchoose) {
					case "czarny": {
						shapes.setFillColor(i, Color.BLACK);
						break;
					}
					case "czerwony": {
						shapes.setFillColor(i, Color.RED);
						break;
					}

					case "niebieski": {
						shapes.setFillColor(i, Color.BLUE);
						break;
					}
					case "pomaranczowy": {
						shapes.setFillColor(i, Color.ORANGE);
						break;
					}
					case "rozowy": {
						shapes.setFillColor(i, Color.PINK);
						break;
					}

					case "zolty": {
						shapes.setFillColor(i, Color.YELLOW);
						break;
					}
					}

				}
			}
		}

		// temp.draw(kwadracik);
		if (count == 0)
			shapes.drawAllPaths(temp);

		// temp.fill(way);
		drawingPanel.setG2d(temp);
		drawingPanel.repaint();
		drawingPanel.revalidate();
	}

	/**
	 * Constructor for DrawingMouseListener
	 * 
	 * @param drawingPanel
	 *            custom panel to get g2d from and draw on
	 * @param internalGUIFrame
	 *            frame to display dialogs on
	 * @param shapes
	 *            object to add and create paths
	 */
	DrawingMouseListener(DrawingPanel drawingPanel,
			JInternalFrame internalGUIFrame, Shapes shapes) {
		this.shapes = shapes;
		this.drawingPanel = drawingPanel;
		this.internalGUIFrame = internalGUIFrame;

	}
}

/**
 * class ResizingListener, responsible for figures resizing
 * 
 * @version 1.1
 * @author d0ku (Jakub Piątkowski)
 */
class ResizingListener implements MouseWheelListener {
	private DrawingPanel drawingPanel;
	private JInternalFrame internalGUIFrame;
	private Shapes shapes;

	/**
	 * resizes figure which is on top and moue is pointed at
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseWheelMoved(MouseWheelEvent e) {

		Graphics2D temp = drawingPanel.getG2d();
		double location_x = 0;
		double location_y = 0;

		if (shapes.getMode() == 1) {

			for (int i = shapes.getCount() - 1; i >= 0; i--) {
				if (shapes.indexedPath(i).contains(e.getX(), e.getY())) {
					temp.setColor(Color.RED);
					AffineTransform scaler = new AffineTransform();
					location_x = e.getX();
					location_y = e.getY();
					//
					if (e.getWheelRotation() < 0) {
						location_x = -(location_x * 1.05 - location_x);
						location_y = -(location_y * 1.05 - location_y);

						scaler.translate(location_x, location_y);

						scaler.scale(1.05, 1.05);
					}
					if (e.getWheelRotation() > 0) {

						location_x = -(location_x * 0.95 - location_x);
						location_y = -(location_y * 0.95 - location_y);

						scaler.translate(location_x, location_y);
						scaler.scale(0.95, 0.95);
					}
					shapes.indexedPath(i).transform(scaler);
					temp.draw(shapes.indexedPath(i));
					temp.setColor(Color.BLACK);
					break;
				}
			}

		}
		shapes.drawAllPaths(temp);
		drawingPanel.setG2d(temp);
		drawingPanel.repaint();
		drawingPanel.revalidate();
		e.getWheelRotation();
	}

	/**
	 * Constructor for ResizingListener
	 * 
	 * @param drawingPanel
	 *            custom Panel to get g2d from and draw on
	 * @param internalGUIFrame
	 *            frame to display dialogs on
	 * @param shapes
	 *            object responsible for paths getting
	 */
	ResizingListener(DrawingPanel drawingPanel,
			JInternalFrame internalGUIFrame, Shapes shapes) {
		this.shapes = shapes;
		this.drawingPanel = drawingPanel;
		this.internalGUIFrame = internalGUIFrame;

	}
}



/**
 * class SaveButtonsEnabler, sets save buttons to enabled, or disabled
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.05
 */
