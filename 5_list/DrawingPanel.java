import javax.swing.*;
import java.awt.*;


/**
 * class DrawingPanel on which everything is drawn
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version beta
 */
@SuppressWarnings("serial")
class DrawingPanel extends JPanel {
	private Graphics2D g2d;

	protected void paintComponent(Graphics g) {
		g2d = (Graphics2D) g.create();
	}

	/**
	 * Getter for DrawingPanel's g2d
	 * 
	 * @return DrawingPanel's g2d
	 */
	public Graphics2D getG2d() {
		return g2d;
	}

	/**
	 * Setter for DrawingPanel's g2d
	 * 
	 * @param g2d
	 *            to be set on Panel
	 */
	public void setG2d(Graphics2D g2d) {
		this.g2d = g2d;
	}
}