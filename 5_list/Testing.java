import javax.swing.*;



/**
 * class Testing, tests the whole app
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.89
 */
@SuppressWarnings("serial")
public class Testing extends JApplet {

	/**
	 * starts the whole app
	 * 
	 * @param args
	 *            they doesnt matter
	 */
	public static void main(String[] args) {

		AppGui whole_app = new AppGui();

		JInternalFrame internalGUIFrame = whole_app.getAppInternalJFrame();

		JFrame okieneczko = new JFrame("okno wlasciwe");
		okieneczko.setBounds(540, 830, 830, 540);
		okieneczko.add(internalGUIFrame);
		okieneczko.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		okieneczko.setVisible(true);
		okieneczko.setResizable(false);

	}

	/**
	 * inits the whole applet
	 */
	public void init() {
		AppGui whole_app = new AppGui();

		JInternalFrame internalGUIFrame = whole_app.getAppInternalJFrame();

		this.add(internalGUIFrame);

		this.setVisible(true);

	}
}