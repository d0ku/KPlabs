import javax.swing.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;

/**
 * class AppGui, creates the whole GUI and stores it in JInternalFrame
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.7
 */
class AppGui {
	private JInternalFrame internalGUIFrame;

	/**
	 * Constructor for AppGui, creates the whole interface
	 */
	AppGui() {
		this.appGuiInit();
	}

	private void appGuiInit() {

		DrawingPanel drawingPanel = new DrawingPanel();
		drawingPanel.setPreferredSize(new Dimension(400, 600));
		drawingPanel.setBorder(BorderFactory.createLineBorder(Color.black));

		Shapes shapes = new Shapes(drawingPanel);

		this.internalGUIFrame = new JInternalFrame("Paint'a'Like");
		// internalGUIFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		internalGUIFrame.setBounds(540, 830, 830, 540);
		internalGUIFrame.setResizable(false);

		JPanel menuPanel = new JPanel(new FlowLayout());

		menuPanel.setBorder(BorderFactory.createLineBorder(Color.black));

		menuPanel.setPreferredSize(new Dimension(20, 35));

		JButton infodisplay = new JButton("O programie");
		infodisplay.addActionListener(new InfoDisplayActionListener(
				internalGUIFrame));

		String[] modes = { "Rysowanie", "Obrabianie" };
		JComboBox<String> which_mode = new JComboBox<String>(modes);
		// which_mode.addItem("Rysowanie");
		// which_mode.addItem("Obrabianie");

		which_mode
				.addItemListener(new WhichModeItemListener(shapes, which_mode));

		String[] types = { "Wielokąt", "Koło", "Prostokąt" };
		JComboBox<String> which_type = new JComboBox<String>(types);
		// which_type.addItem("Wielokat");
		// which_type.addItem("Kolo");
		// which_type.addItem("Prostokat");

		which_type
				.addItemListener(new WhichTypeItemListener(shapes, which_type));

		JButton backup_save = new JButton("Zapisz");
		backup_save.addActionListener(new BackupSaverListener(shapes));
		JButton backup_read = new JButton("Wczytaj");
		backup_read.addActionListener(new BackupReaderListener(shapes));

		JButton backup_save_single = new JButton("Zapisz ostatnią");
		backup_save_single.addActionListener(new BackupSaverSingleListener(
				shapes, internalGUIFrame));
		JButton backup_read_single = new JButton("Wczytaj jedną");
		backup_read_single.addActionListener(new BackupReaderSingleListener(
				shapes, internalGUIFrame));

		menuPanel.add(which_mode);
		menuPanel.add(which_type);

		menuPanel.add(backup_save);
		menuPanel.add(backup_save_single);
		menuPanel.add(backup_read);

		backup_save_single.setEnabled(false);
		backup_save.setEnabled(false);

		menuPanel.add(backup_read_single);

		menuPanel.add(infodisplay);

		internalGUIFrame.getContentPane().setLayout(
				new BoxLayout(internalGUIFrame.getContentPane(),
						BoxLayout.PAGE_AXIS));
		internalGUIFrame.add(menuPanel);
		internalGUIFrame.add(drawingPanel);

		ButtonPressedListener which_button = new ButtonPressedListener();

		drawingPanel.addMouseListener(new DrawingMouseListener(drawingPanel,
				internalGUIFrame, shapes));
		drawingPanel.addMouseListener(which_button);
		drawingPanel.addMouseWheelListener(new ResizingListener(drawingPanel,
				internalGUIFrame, shapes));
		drawingPanel.addMouseMotionListener(new MyMouseMotionListener(
				drawingPanel, internalGUIFrame, shapes, which_button));
		drawingPanel.addMouseMotionListener(new SaveButtonsEnabler(
				backup_save_single, backup_save, shapes));
		internalGUIFrame.setVisible(true);
	}

	/**
	 * Getter for JInternalFrame, which contains the whole GUI
	 * 
	 * @return whole GUI JInternalFrame
	 */
	public JInternalFrame getAppInternalJFrame() {
		return this.internalGUIFrame;
	}
}

/**
 * class WhichModeItemListener, responsible for setting modes in Shapes object
 * 
 * @version 1.0
 * @author d0ku (Jakub Piątkowski)
 */
class WhichModeItemListener implements ItemListener {
	private Shapes shapes;
	private JComboBox<String> choice;

	/**
	 * sets modes in Shapes object
	 * 
	 * @param e
	 */
	public void itemStateChanged(ItemEvent e) {

		switch (choice.getSelectedIndex()) {

		case 0: {
			shapes.setMode(0);
			shapes.setSafe(false);
			break;
		}
		case 1: {
			shapes.setMode(1);
			shapes.setSafe(false);
			break;
		}
		default:{
			shapes.setMode(1);
			shapes.setSafe(false);
			break;
		}
		}
	}

	/**
	 * Constructor for WhichModeItemListener
	 * 
	 * @param shapes
	 *            Shapes object to set modes on
	 * @param choice
	 *            JComboBox to get mode index from
	 */
	WhichModeItemListener(Shapes shapes, JComboBox<String> choice) {
		this.shapes = shapes;
		this.choice = choice;
	}
}

/**
 * class WhichTypeItemListener, sets Type on Shapes object
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.0
 */
class WhichTypeItemListener implements ItemListener {
	private Shapes shapes;
	private JComboBox<String> choice;

	/**
	 * sets Type on Shapes object
	 * 
	 * @param e
	 */
	public void itemStateChanged(ItemEvent e) {

		switch (choice.getSelectedIndex()) {

		case 0: {
			shapes.setType(0);
			shapes.setSafe(false);
			break;
		}
		case 1: {
			shapes.setType(1);
			shapes.setSafe(false);
			break;
		}
		case 2: {
			shapes.setType(2);
			shapes.setSafe(false);
			break;
		}
		default:{
			shapes.setType(2);
			break;
		}
		}
	}

	/**
	 * Constructor for WhichTypeItemListener
	 * 
	 * @param shapes
	 *            Shapes to set Type on
	 * @param choice
	 *            JComboBox to get Type index from
	 */
	WhichTypeItemListener(Shapes shapes, JComboBox<String> choice) {
		this.shapes = shapes;
		this.choice = choice;
	}
}

/**
 * class InfoDisplayActionListener, displays aboutdialog
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.1
 */
class InfoDisplayActionListener implements ActionListener {
	private JInternalFrame internalGUIFrame;

	/**
	 * displays dialog
	 * 
	 * @param e
	 *            ActionEvent
	 */
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(
				internalGUIFrame,
				"Paint'a'Like - marna podróbka Painta :)"
						+ System.lineSeparator()
						+ "Autor: d0ku (Jakub Piątkowski)"
						+ System.lineSeparator() + "Wersja: beta 4ever",
				"O programie", JOptionPane.INFORMATION_MESSAGE);

	}

	/**
	 * Constructor for InfoDisplayActionListener
	 * 
	 * @param internalGUIFrame
	 *            frame to display dialog on
	 */
	InfoDisplayActionListener(JInternalFrame internalGUIFrame) {
		this.internalGUIFrame = internalGUIFrame;
	}
}

/**
 * class BackupReaderListener, for all paths from Shapes backup
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.0
 */
class BackupReaderListener implements ActionListener {
	private Shapes shapes;

	/**
	 * activates Shapes loadAllPaths() method
	 * 
	 * @param e
	 *            ActionEvent, button must be pressed
	 */
	public void actionPerformed(ActionEvent e) {

		shapes.loadAllPaths();
	}

	/**
	 * Constructor for BackupReaderListener
	 * 
	 * @param shapes
	 *            Shapes object to perform loadAllPaths() method
	 */
	BackupReaderListener(Shapes shapes) {
		this.shapes = shapes;
	}
}

/**
 * class BackupSaverListener, for all paths backup from Shapes
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.1
 */
class BackupSaverListener implements ActionListener {
	private Shapes shapes;

	/**
	 * calls backupAllPaths() method from Shapes object
	 * 
	 * @param e
	 *            ActionEvent, button must be pressed
	 */
	public void actionPerformed(ActionEvent e) {

		shapes.backupAllPaths();
	}

	/**
	 * Constructor for BackupSaverListener
	 * 
	 * @param shapes
	 *            Shapes object to perform backupAllPaths() method on
	 */
	BackupSaverListener(Shapes shapes) {
		this.shapes = shapes;
	}
}

/**
 * class BackupReaderSingleListener, allows user to choose file location and
 * load path
 * 
 * @author d0ku (Jakub Piątkowski)
 * @version 1.2
 */
class BackupReaderSingleListener implements ActionListener {
	private Shapes shapes;
	private JInternalFrame internalGUIFrame;

	/**
	 * creates JFileChooser dialog and provides location to Shapes
	 * loadOnePath(location) method
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {

		try {
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showOpenDialog(internalGUIFrame);
			if (returnVal == JFileChooser.APPROVE_OPTION)
				shapes.loadOnePath(chooser.getCurrentDirectory()
						.getAbsolutePath()
						+ "/"
						+ chooser.getSelectedFile().getName());
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(internalGUIFrame,
					"Nie mozna uruchomic panelu wczytywania",
					"Błąd wczytywania", JOptionPane.WARNING_MESSAGE);

		}
	}

	/**
	 * Constructor for BackupReaderSingleListener
	 * 
	 * @param shapes
	 *            Shapes to call loadOnePath(location) method on
	 * @param internalGUIFrame
	 *            frame to display dialog on
	 */
	BackupReaderSingleListener(Shapes shapes, JInternalFrame internalGUIFrame) {
		this.shapes = shapes;
		this.internalGUIFrame = internalGUIFrame;
	}
}

/**
 * class BackupSaverSingleListener, allows user to choose location and save last
 * path
 * @version 1.1
 * @author d0ku (Jakub Piątkowski)
 */
class BackupSaverSingleListener implements ActionListener {
	private Shapes shapes;
	private JInternalFrame internalGUIFrame;

	/**
	 * creates JFileChooser dialog and provides location from it to Shapes
	 * saveLastPath(location) method
	 * 
	 * @param e
	 *            ActionEvent, button pressed
	 */
	public void actionPerformed(ActionEvent e) {

		try {
			JFileChooser chooser = new JFileChooser();
			int returnVal = chooser.showSaveDialog(internalGUIFrame);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				System.out.println(chooser.getCurrentDirectory()
						.getAbsolutePath()
						+ "/"
						+ chooser.getSelectedFile().getName());
				shapes.saveLastPath(chooser.getCurrentDirectory()
						.getAbsolutePath()
						+ "/"
						+ chooser.getSelectedFile().getName());
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(internalGUIFrame,
					"Nie mozna uruchomic panelu zapisywania", "Błąd zapisu",
					JOptionPane.WARNING_MESSAGE);
		}

	}

	/**
	 * Constructor for BackupSaverSingleListener
	 * 
	 * @param shapes
	 *            Shapes to call saveLastPath(location) method from
	 * @param internalGUIFrame
	 *            frame to display dialog on
	 */
	BackupSaverSingleListener(Shapes shapes, JInternalFrame internalGUIFrame) {
		this.shapes = shapes;
		this.internalGUIFrame = internalGUIFrame;
	}
}

class SaveButtonsEnabler implements MouseMotionListener {

	private JButton backup_save_single;

	private JButton backup_save;
	private Shapes shapes;

	/**
	 * when mouseMoved checks whether figure to save exists, sets save buttons
	 * enabled if true
	 * 
	 * @param e
	 *            MouseEvent
	 */
	public void mouseMoved(MouseEvent e) {
		if (shapes.getCount() == 0) {

			backup_save_single.setEnabled(false);
			backup_save.setEnabled(false);

		} else {

			backup_save_single.setEnabled(true);
			backup_save.setEnabled(true);

		}
	}

	public void mouseDragged(MouseEvent e) {
	}

	/**
	 * Constructor for SaveButtonsEnabler
	 * 
	 * @param backup_save_single
	 *            button to be enabled or disabled
	 * @param backup_save
	 *            button to be disabled or enabled
	 * @param shapes
	 *            Shapes object to get shapes count from
	 */
	SaveButtonsEnabler(JButton backup_save_single, JButton backup_save,
			Shapes shapes) {

		this.backup_save_single = backup_save_single;
		this.backup_save = backup_save;
		this.shapes = shapes;
	}
}