/** class Java_light, main method
@author d0ku (Jakub Piątkowski)
@version 1.0
 */
import java.lang.*;

class Java_light {

	public static void main(String[] args) {

		if(args.length<=0)
			throw new IllegalArgumentException("Brak argumentow wejscia ");

		int count = 0;
		double f = 0, b = 0, c = 0, d = 0, e = 0;

		int temp_czworokat = 0;

		char[] figury = args[0].toCharArray();

		for (int i = 0; i < figury.length; i++) {

			if ((figury[i] == 'o') || (figury[i] == 'p') || (figury[i] == 's'))
				count++;
			if (figury[i] == 'c')
				count = count + 5;

		}

		if (count < args.length - 1)
			throw new IllegalArgumentException(
					"Niewystarczajaca ilosc parametrow, brakuje: "
							+ (count - args.length + 1));

		count = 1;

		Figura[] tab = new Figura[figury.length];

		for (int i = 0; i < figury.length; i++) {

			if (figury[i] == 'o') {

				f = Java_light.parsowanie(args[count]);
				Figura temp = new Figura();
				Figura.Rodzaj_1.OKRAG.setter(f);
				temp.pole_set(Figura.Rodzaj_1.OKRAG.pole());
				temp.obwod_set(Figura.Rodzaj_1.OKRAG.obwod());

				tab[i] = temp;
				count++;
				temp = null;

			} else if (figury[i] == 'c') {

				f = Java_light.parsowanie(args[count]);
				b = Java_light.parsowanie(args[count + 1]);
				c = Java_light.parsowanie(args[count + 2]);
				d = Java_light.parsowanie(args[count + 3]);
				e = Java_light.parsowanie(args[count + 4]);

				Figura temp = new Figura();
				temp_czworokat = czworokat_maker(f, b, c, d, e);

				switch (temp_czworokat) {

				case 1:
					Figura.Rodzaj_2.PROSTOKAT.setter(f, b);
					temp.pole_set(Figura.Rodzaj_2.PROSTOKAT.pole());
					temp.obwod_set(Figura.Rodzaj_2.PROSTOKAT.obwod());
					break;
				case 2:
					Figura.Rodzaj_2.PROSTOKAT.setter(f, c);
					temp.pole_set(Figura.Rodzaj_2.PROSTOKAT.pole());
					temp.obwod_set(Figura.Rodzaj_2.PROSTOKAT.obwod());
					break;
				case 3:
					Figura.Rodzaj_2.PROSTOKAT.setter(f, c);
					temp.pole_set(Figura.Rodzaj_2.PROSTOKAT.pole());
					temp.obwod_set(Figura.Rodzaj_2.PROSTOKAT.obwod());
					break;
				case 4:
					Figura.Rodzaj_2.ROMB.setter(f, b);
					temp.pole_set(Figura.Rodzaj_2.ROMB.pole());
					temp.obwod_set(Figura.Rodzaj_2.ROMB.obwod());
					break;
				case 5:
					Figura.Rodzaj_1.KWADRAT.setter(f);
					temp.pole_set(Figura.Rodzaj_1.KWADRAT.pole());
					temp.obwod_set(Figura.Rodzaj_1.KWADRAT.obwod());
					break;

				case 0:
					throw new IllegalArgumentException(
							"Dla podanych danych nie ma funcji liczącej pole i obwod czworokata "
									+ f + " " + b + " " + c + " " + d + " " + e);

				}

				tab[i] = temp;
				count = count + 5;
				temp = null;
			} else if (figury[i] == 'p') {
				f = Java_light.parsowanie(args[count]);
				Figura temp = new Figura();
				Figura.Rodzaj_1.PIECIOKAT.setter(f);
				temp.pole_set(Figura.Rodzaj_1.PIECIOKAT.pole());
				temp.obwod_set(Figura.Rodzaj_1.PIECIOKAT.obwod());

				tab[i] = temp;
				count++;
				temp = null;
			} else if (figury[i] == 's') {
				f = Java_light.parsowanie(args[count]);
				Figura temp = new Figura();
				Figura.Rodzaj_1.SZESCIOKAT.setter(f);
				temp.pole_set(Figura.Rodzaj_1.SZESCIOKAT.pole());
				temp.obwod_set(Figura.Rodzaj_1.SZESCIOKAT.obwod());

				tab[i] = temp;
				count++;
				temp = null;

			}
			else
				throw new IllegalArgumentException("Dla tego znaku nie rozpoznano figury: "+figury[i]);

		}

		for (int i = 1; i < figury.length + 1; i++) {
			System.out.println("Pole " + i + " figury wynosi "
					+ tab[i - 1].pole());
			System.out.println("Obwod " + i + " figury wynosi "
					+ tab[i - 1].obwod());

		}

	}

	private static double parsowanie(String a) {
		double z = 0;

		try {
			z = Double.parseDouble(a);
		} catch (NumberFormatException ex) {
			System.out.println("To nie jest liczba: " + a);
		}

		return z;
	}

	private static int czworokat_maker(double a, double b, double c, double d,
			double angle) {

		if (!(Math.max(Math.max(a, b), Math.max(c, d)) < a + b + c + d
				- Math.max(Math.max(a, b), Math.max(c, d))))
			throw new IllegalArgumentException(
					"Te liczby nie spelniaja warunku czworokata ");

		if (a <= 0 || b <= 0 || c <= 0 || d <= 0)
			throw new IllegalArgumentException(
					"Jeden z bokow czworokata jest mniejszy od zera: ");

		if (angle <= 0 || angle >= 180)
			throw new IllegalArgumentException("Kat czworokata jest bledny: "
					+ angle);

		if (a == c && b == d && a == b && angle == 90) { // warunek na kwadrat
			return 5;
		}

		if (a == c && b == d && angle == 90) { // warunek na prostokat nr 1

			return 1;
		}

		else if (a == b && c == d && angle == 90) { // warunek na prostokat nr 2

			return 2;
		}

		else if (a == d && c == b && angle == 90) { // warunek na prostokat nr 3

			return 3;
		}

		if (a == b && c == d && a == c && !(angle == 90.0)) { // romb
			return 4;

		}
		return 0;
	}

}