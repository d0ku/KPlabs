/**Figura, class containing both enums
@author d0ku (Jakub Piątkowski)
@version 1.0
 */
import java.lang.*;

class Figura {

	private double pole;
	private double obwod;

	public double obwod() {
		return this.obwod;
	}

	public double pole() {
		return this.pole;
	}

	public void pole_set(double a) {
		this.pole = a;
	}

	public void obwod_set(double a) {
		this.obwod = a;
	}

	public enum Rodzaj_1 {
		SZESCIOKAT {

			public void setter(double a) {
				if(a==0)
					throw new IllegalArgumentException("Bok musi byc wiekszy od zera: "+a);
				this.a = a;
			}

			public double pole() {
				this.pole = this.a * this.a * Math.sqrt(3) * 6 / 4;
				return this.pole;

			}

			public double obwod() {
				this.obwod = 6 * this.a;
				return this.obwod;
			}

		},
		PIECIOKAT {

			public void setter(double a) {
				if(a==0)
					throw new IllegalArgumentException("Bok musi byc wiekszy od zera: "+a);
				this.a = a;
			}

			public double pole() {
				this.pole = ((this.a * this.a) * (Math.sqrt(5 * (5 + 2 * Math.sqrt(5))))) * 0.25;

				return this.pole;
			}

			public double obwod() {
				this.obwod = this.a * 5;

				return this.obwod;
			}

		},
		KWADRAT {

			public void setter(double a) {
				this.a = a;
			}

			public double pole() {
				this.pole = this.a * this.a;

				return this.pole;
			}

			public double obwod() {
				this.obwod = this.a * 4;

				return this.obwod;
			}

		},
		OKRAG {

			public void setter(double a) {
				if(a==0)
					throw new IllegalArgumentException("Promien musi byc wiekszy od zera: "+a);
				this.a = a;
			}

			public double pole() {
				this.pole = this.a * this.a * Math.PI;

				return this.pole;
			}

			public double obwod() {
				this.obwod = 2 * this.a * Math.PI;

				return this.obwod;
			}

		};

		protected double obwod;
		protected double pole ;
		protected double a;

		public abstract double pole();

		public abstract double obwod();

		public abstract void setter(double a);

	}

	public enum Rodzaj_2 {
		PROSTOKAT {

			public void setter(double a, double b) {
				this.a = a;
				this.b = b;
			}

			public double pole() {
				this.pole = this.a * this.b;
				return this.pole;

			}

			public double obwod() {
				this.obwod = 2 * this.a + 2 * this.b;
				return this.obwod;
			}
		},
		ROMB {

			public void setter(double a, double b) {
				this.a = a;
				this.b = b;

			}

			public double pole() {
				this.pole = this.a * this.a * Math.sin(Math.toRadians(this.b));
				return this.pole;

			}

			public double obwod() {
				this.obwod = 4 * this.a;
				return this.obwod;
			}
		};

		protected double obwod;
		protected double pole;
		protected double a;
		protected double b;

		public abstract double pole();

		public abstract double obwod();

		public abstract void setter(double a, double b);

	}

}