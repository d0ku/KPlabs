import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


/**
 * class UniversalTextFieldListener, empties the JTextfield when clicked on
 * @author d0ku
 */
class UniversalTextFieldListener implements FocusListener {
	JTextField field;
	/**
	 * Empties the field when clicked
	 * @param e [description]
	 */
	public void focusGained(FocusEvent e) {
		field.setText("");
	}

	public void focusLost(FocusEvent e) {}

	/**
	 * Constructor
	 * @param  field field to be emptied
	 */
	UniversalTextFieldListener(JTextField field) {
		this.field = field;

	}
}

/**
 * class AboutListener, displays About dialog
 * @author d0ku
 */
class AboutListener implements ActionListener {
	JFrame window;

	/**
	 * Shows the About dialog
	 * @param e when About button pushed
	 */
	public void actionPerformed(ActionEvent e) {

		JOptionPane.showMessageDialog(window,
				"Program do uruchamiania symulacji z wilkami by d0ku");
	}

	/**
	 * Constructor
	 * @param  window JFrame to display Dialog on
	 */
	AboutListener(JFrame window) {
		this.window = window;
	}
}

/**
 * class StarterListener, it starts the SheepandBeep with parameters from JTextFields
 * @author  d0ku
 */
class StarterListener implements ActionListener {
	String rabbit;
	String k;
	String y;
	String x;
	JFrame window;
	int temp0 = 0;
	int temp1 = 0;
	int temp2 = 0;
	int temp3 = 0;
	JTextField a, b, c, d;

	/**
	 * starts SheepandBeep with parameters from JTextFields
	 * @param e run button clicked
	 */
	public void actionPerformed(ActionEvent e) {
		rabbit = a.getText();
		k = b.getText();
		y = c.getText();
		x = d.getText();

		try {
			temp0 = Integer.parseInt(rabbit);
			temp1 = Integer.parseInt(k) * 1000;
			temp2 = Integer.parseInt(y);
			temp3 = Integer.parseInt(x);
		} catch (Exception ex) {

			JOptionPane.showMessageDialog(window,
					"Conajmniej jeden z argumentów nie jest liczbą...");
			return;
		}

		if (temp0 < 0 || temp1 < 0 || temp2 <= 0 || temp3 <= 0) {
			JOptionPane.showMessageDialog(window,
					"Wartość argumentów jest za niska...");
			return;
		}

		if (rabbit != "" && k != "" && y != "" && x != "")
			try {
				this.window.dispose();
				Process compile = Runtime.getRuntime().exec("javac SheepAndBeep.java");
				Process proces = Runtime.getRuntime().exec(
						"java SheepAndBeep " + rabbit + " " + k + " " + y + " " + x);
			} catch (Exception ex) {
				System.out.println("nie działa");
			}

		else
			JOptionPane.showMessageDialog(window,
					"Nie wszystkie pola są uzupełnione");
	}

	/**
	 * Constructor
	 * @param  window temporal JFrame to dispose or display dialog on
	 * @param  a JTextField containing Rabbit count
	 * @param  b JTextField containing latency
	 * @param  c JTextField containing ysize
	 * @param  d JTextField containing xsize
	 */
	StarterListener(JFrame window,
			JTextField a, JTextField b, JTextField c, JTextField d) {
		this.window = window;
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
}

/**
 * class Teststarter, just a GUI starter for SheepandBeep
 */
public class Teststarter {

	/**
	 * whole stuff
	 * @param args input from terminal, does nothing
	 */
	public static void main(String[] args) {
		JFrame window = new JFrame("Starter");
		window.setBounds(300, 200, 200, 300);
		window.setVisible(true);
		window.setResizable(false);

		Boolean[] safe = { false, false, false, false };

		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());

		JButton about = new JButton("About");
		about.addActionListener(new AboutListener(window));
		JTextField rabbit_count = new JTextField("Tu wpisz liczbę zajęcy ");
		rabbit_count.addFocusListener(new UniversalTextFieldListener(
				rabbit_count));
		JTextField latency = new JTextField("Tu wpisz opóźnienie");
		latency.addFocusListener(new UniversalTextFieldListener(latency));
		JTextField sizey = new JTextField("Tu wpisz rozmiar y planszy");
		sizey.addFocusListener(new UniversalTextFieldListener(sizey));
		JTextField sizex = new JTextField("Tu wpisz rozmiar x planszy");
		sizex.addFocusListener(new UniversalTextFieldListener(sizex));

		JButton starter = new JButton("Uruchom");
		starter.addActionListener(new StarterListener(window,
				rabbit_count, latency, sizey, sizex));

		panel.add(about);
		panel.add(rabbit_count);
		panel.add(latency);
		panel.add(sizey);
		panel.add(sizex);
		panel.add(starter);

		window.add(panel);
		window.pack();

	}
}