/** Thirdexercise, finds the biggest divider of n
@author d0kus (Jakub Piątkowski)
@version 1.1
*/
public class Thirdexercise
{

	public static void main(String[] args)
		{
			int n=0;
			for (int i=0;i<args.length;i++) 
			{
				try { n=Integer.parseInt(args[i]); }
				catch (NumberFormatException ex) {
					System.out.println(args[i] + " nie jest liczba calkowita");
					continue;
				}

				n=div(n);
				if ((n!=0) && (n!=(-1)))
				System.out.println(n);
				else if (n!=(-1))
					System.out.println("Ta liczba nie posiada mniejszych dzielnikow");
				else 
					System.out.println("Liczba jest ujemna");
			}
		}

	public static int div(int n)
		{
			if (n<0)
				return -1;

			if ((n==0) || (n==1))
				return 0;

			for (int i=2;i<=Math.sqrt(n);i++)
				if (n%i==0)
					return n/i;

			return 1;
		}
}